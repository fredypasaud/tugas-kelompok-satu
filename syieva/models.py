from django.db import models
from datetime import datetime, date

class Feedback(models.Model):
    name = models.CharField(max_length = 20)
    message = models.CharField(max_length = 100)
    msgDetails = models.DateTimeField(default = datetime.now)

# Def getDateTime(self)