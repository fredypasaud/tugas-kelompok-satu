from django.test import TestCase, RequestFactory, Client
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from . import views

class UnitTestcase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
        username='jacob', email='jacob@…', password='top_secret')

    def test_details(self):
        request = self.factory.get('/login/')
        request.user = self.user
        response = views.user_login(request)
        userlog = authenticate(request.user)
        self.assertEqual(response.status_code, 200)
