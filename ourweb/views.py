from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import login, authenticate,logout

response = {}

def index(request):
	return render(request,'OK.html')

def user_login(request):
    # response['login_view'] = "active"
    if request.method == "POST":
         username = request.POST['username']
         password = request.POST['password']
         user = authenticate(request, username=username, password=password)
         if user is not None:
            login(request, user)
            return redirect('OK')
         else:
            return HttpResponse("Invalid Password or Username")
    else:
        return render(request, 'login.html')

def user_logout(request):
    request.session.flush()
    logout(request)
    return redirect('login')
