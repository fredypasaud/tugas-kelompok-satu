from django.contrib import admin
from .models import Engineer, Report

admin.site.register(Engineer)
admin.site.register(Report)

# Register your models here.
