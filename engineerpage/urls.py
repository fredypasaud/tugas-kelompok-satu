from django.urls import include,path
from . import views

urlpatterns  = [
    path('', views.engineer, name = 'overview'),
    path('create/', views.engineer_create, name = 'engineer_create'),
    path('<int:pk>', views.engineer_detail, name = 'engineer_detail'),
    path('report/', views.report, name = 'report_overview'),
    path('report/create/', views.report_create, name = 'report_create'),
    path('report/<int:pk>', views.report_detail, name = 'report_detail'),
]
