from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Engineer, Report
from .forms import EngineerForms, ReportForms

# def overview(request):
#     return(request, 'overview_engineer.html')

def engineer_create(request):
    if request.method == "POST":
        form = EngineerForms(request.POST)
        if form.is_valid():
            engineer = form.save(commit=False)
            engineer.save()
            return redirect('engineer_detail', pk=engineer.pk)
    else:
        form = EngineerForms()
    return render(request, 'engineer.html', {'form' : form,})

def engineer_detail(request,pk):
    engineer = Engineer.objects.get(pk=pk)
    return render(request, 'engineer_details.html', {'engineer' : engineer})

def engineer(request):
    engineers = Engineer.objects.all()
    return render(request, 'engineer_list.html', {'engineers' : engineers})

def report(request):
    reports = Report.objects.all()
    return render(request, 'report_list.html', {'reports' : reports})

def report_detail(request,pk):
    report = Report.objects.get(pk=pk)
    return render(request, 'report_detail.html', {'report' : report})

def report_create(request):
    if request.method == "POST":
        formr = ReportForms(request.POST)
        if formr.is_valid():
            report = formr.save(commit=False)
            report.save()
            return redirect('report_detail', pk=report.pk)
    else:
        formr = ReportForms()
    return render(request, 'report.html', {'formr' : formr,})
