from django import forms
from .models import Engineer, Report
from django.forms import widgets

class EngineerForms (forms.ModelForm):

    class Meta:
        model = Engineer
        fields = {"namaEngineer", "divisiEngineer", "lokasiEngineer", "status"}
    field_order = ["namaEngineer", "divisiEngineer", "lokasiEngineer", "status"]

class ReportForms (forms.ModelForm):

    class Meta:
        model = Report
        fields = {"namaPembuatReport", "jenisReport", "detailReport"}
    field_order = ["namaPembuatReport", "jenisReport", "detailReport"]
