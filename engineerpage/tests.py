from django.test import TestCase
from .models import Engineer, Report
from . import views
from django.urls import reverse
from .forms import EngineerForms, ReportForms
from datetime import datetime, timezone

class EngineerTest(TestCase):

    def createEngineer(self, name = "Aryo", divisi = "LKDE", lokasi = "Jakarta Utara", status = "Available" ):
        return Engineer.objects.create(namaEngineer = name, divisiEngineer = divisi, lokasiEngineer = lokasi, status = status)

    def createReport(self):
        name = "Aryo"
        jenisReport = "Error Report"
        detailReport = "Ada error mesin yang terdeteksi"
        return Report.objects.create(namaPembuatReport = name, jenisReport = jenisReport, detailReport = detailReport)

    def testEng(self):
        c = self.createEngineer()
        self.assertTrue(isinstance(c, Engineer))
        self.assertEqual(c.__str__(), c.namaEngineer)
        url = reverse('overview')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)


    def testReport(self):
        r = self.createReport()
        self.assertTrue(isinstance(r, Report))
        self.assertEqual(r.__str__(), r.namaPembuatReport)
        url = reverse('report_overview')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_eng_det(self):
        c = self.createEngineer()
        resp = self.client.get('/engineer/1')
        self.assertEqual(resp.status_code, 200)

    def test_rep_det(self):
        c = self.createReport()
        resp = self.client.get('/engineer/report/1')
        self.assertEqual(resp.status_code, 200)

    def test_eng_cre(self):
        form_data = {
        "namaEngineer" : "Aryo",
        "divisiEngineer" : "TLE",
        "lokasiEngineer" : "Surabaya",
        "status" : "Available",
        }
        form = EngineerForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/engineer/create/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/engineer/create/')
        self.assertEqual(response.status_code, 200)

    def test_rep_cre(self):
        form_data = {"namaPembuatReport" : "Aryo","jenisReport" : "TLE","detailReport" : "Ini adalah sebuah report",}
        form = ReportForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/engineer/report/create/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/engineer/report/create/')
        self.assertEqual(response.status_code, 200)
