# Generated by Django 2.0.5 on 2019-10-16 16:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Engineer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('namaEngineer', models.CharField(max_length=30)),
                ('divisiEngineer', models.CharField(max_length=30)),
                ('lokasiEngineer', models.CharField(max_length=30)),
                ('status', models.CharField(max_length=10)),
            ],
        ),
    ]
