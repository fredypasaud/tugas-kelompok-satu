from django.db import models

class Engineer(models.Model):
    namaEngineer = models.CharField("Nama Engineer", max_length = 30)
    divisiEngineer = models.CharField("Divisi Engineer", max_length = 30)
    lokasiEngineer = models.CharField("Lokasi Engineer", max_length = 30)
    status = models.CharField("Status", max_length = 10)

    def __str__(self):
        return self.namaEngineer

class Report(models.Model):
    namaPembuatReport = models.CharField("Nama Pembuat Report", max_length = 30)
    jenisReport = models.CharField("Jenis Report", max_length = 30)
    tanggalPembuatan = models.DateTimeField("Tanggal Pembuatan", auto_now_add = True)
    detailReport = models.CharField("Detail Report", max_length = 500)

    def __str__(self):
        return self.namaPembuatReport
