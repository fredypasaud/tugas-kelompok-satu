# Generated by Django 2.2.6 on 2019-10-19 06:04

import django.core.validators
from django.db import migrations, models
import overNmachsignuppage.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Machine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('machine_name', models.CharField(max_length=200)),
                ('machine_type', models.CharField(max_length=200)),
                ('manufacturer', models.CharField(max_length=200)),
                ('year_built', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(overNmachsignuppage.models.getTodayYear)])),
                ('location', models.CharField(max_length=200)),
                ('status', models.IntegerField(validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(3)])),
            ],
        ),
    ]
