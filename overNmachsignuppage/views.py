from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import FormSignUpMesin
from .models import Machine
import datetime
from random import randint

# Create your views here.

def index(request):
	return redirect('overview')

def overview(request):
	AllMachine = Machine.objects.all()
	ErrorMachines = [i for i in AllMachine if i.getStatus()==3]
	RepairingMachines = [i for i in AllMachine if i.getStatus()==2]
	SafeMachines = [i for i in AllMachine if i.getStatus()==1]
	ErrCount = len(ErrorMachines)
	RepCount = len(RepairingMachines)
	SafeCount = len(SafeMachines)
	return render(request,'overview.html', {"ErrorM":ErrorMachines, "RepairM":RepairingMachines, "SafeM":SafeMachines, "ErrC":ErrCount, "RepC":RepCount, "SafeC":SafeCount})

def signUpMesin(request):
	if request.method == "GET":
		forms = FormSignUpMesin()
		return render(request, 'signUpMesin.html', {"form":forms})
	elif request.method == "POST":
		b = datetime.date.today()
		form = FormSignUpMesin(request.POST)
		if form.is_valid():
			cleanMachineName = form.cleaned_data["machine_name"]
			cleanMachineType = form.cleaned_data["machine_type"]
			cleanManufacturer = form.cleaned_data["manufacturer"]
			cleanYearBuilt = form.cleaned_data["year_built"]
			cleanLocation = form.cleaned_data["location"]
			t = Machine(machine_name=cleanMachineName, machine_type=cleanMachineType, manufacturer=cleanManufacturer, year_built=cleanYearBuilt, location=cleanLocation, status=randint(1, 3))
			t.save()
			return redirect('machoverview')
		return HttpResponse("<h1>Hol Up!</h1><p> What Year is it?</p><a href='goback'>We need to go wayyyyy back!</a>")

def timeTravel(request):
	return redirect('signUpMesin')

def see_engine(request,id):
	machine = Machine.objects.get(id=id)
	return render(request, 'details.html', {'machine_det' : machine})
